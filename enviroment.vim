" Automaticamente el editor entra en modo INSERT
autocmd VimEnter * startinsert

" Iniciar automaticamente NERDTree cuando inicie NVIM
autocmd VimEnter * NERDTree | wincmd p

set number									" Barra lateral de Lineas
set relativenumber					" Numero Relativo
set ruler										" No se :V

