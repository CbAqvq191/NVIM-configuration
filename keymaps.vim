" Abrir este archivo
" Ctrl K S
nnoremap <C-k><C-s> :e ~\AppData\Local\nvim\keymaps.vim<CR> 

" NerdTree 
" Ctrl K E
nnoremap <C-k><C-e> :NERDTree<CR>
inoremap <C-k><C-e> <Esc>:NERDTree<CR>

" Guardado
" Ctrl S
inoremap <C-s> <Esc>:w<CR>i
nnoremap <C-s> :w<CR>

" Cambios de pestañas del Split
" Ctrl PgUp  /  Ctrl PgDn
inoremap <C-PageDown> <Esc><C-W>w<CR>i
nnoremap <C-PageDown> <C-W>w<CR>
inoremap <C-PageUp> <Esc><C-W>W<CR>i
nnoremap <C-PageUp> <C-W>W<CR>

" Salir del archivo (SIN GUARDAR)
" Ctrl W
inoremap <C-w> <Esc>:q!<CR>
nnoremap <C-w> :q!<CR>

" Undo / Deshacer
" Ctrl Z
inoremap <C-z> <Esc>:undo<CR>i
nnoremap <C-z> :undo<CR>

" Redo / Rehacer
" Ctrl Y
inoremap <C-y> <Esc>:redo<CR>i
nnoremap <C-y> :redo<CR>

" Borrar linea
" Alt Del
inoremap <M-Delete> <C-O>dd
nnoremap <M-Delete> ddi

" Cambio de linea
" Alt Arriba / Alt Abajo
nnoremap <C-Up> <Esc>ddkkp
inoremap <C-Up> <Esc>ddkkpi
nnoremap <C-Down> <Esc>ddp
inoremap <C-Down> <Esc>ddpi

" Saltos de linea (Movimiento)
" PgUp (Ins: 10)(Nor: 50)
" PgDn (Ins: 10)(Nor: 50)
inoremap <PageDown> <Esc>10j<C-e>i
inoremap <PageUp> <Esc>10k<C-y>i
nnoremap <PageDown> 50j<C-e>
nnoremap <PageUp> 50k<C-y>

" Resolver identacion
" Ctrl K F
inoremap <C-k><C-f> <Esc>gg=G<CR>i
nnoremap <C-k><C-f> gg=G<CR>

" Buscar en el archivo
" Ctrl F
nnoremap <C-f> /
inoremap <C-f> <Esc>/

" Buscar por NERDTree
" Alt F
inoremap <M-f> <Esc>:NERDTree<CR>:NERDTreeFind 
nnoremap <M-f> :NERDTree<CR>:NERDTreeFind 

" Seleccionar Linea
" Ctrl L
nnoremap <C-l> 0v$
inoremap <C-l> <Esc>0v$

" Abrir una terminal debajo
" Alt T
inoremap <M-t> <Esc>:sp<CR><C-W>w<CR><Esc>:terminal<CR>i
nnoremap <M-t> :sp<CR><C-W>w<CR><Esc>:terminal<CR>i
inoremap <S-M-t> <Esc>:vsp<CR><C-W>w<CR><Esc>:terminal<CR>i
nnoremap <S-M-t> :vsp<CR><C-W>w<CR><Esc>:terminal<CR>i

" Formato Completo para Seleccionar Texto
" Shift Arriba (Cambia a modo Visual)
" Shift Abajo  (Cambia a modo Visual)
" Shift Izquierta (Cambia a modo Visual)
" Shift Derecha (Cambia a modo Visual)
inoremap <S-down> <Esc>v<down>
inoremap <S-up> <Esc>v<up>
inoremap <S-left> <Esc>v<left>
inoremap <S-right> <Esc>v<right>
nnoremap <S-down> <Esc>v<down>
nnoremap <S-up> <Esc>v<up>
nnoremap <S-left> <Esc>v<left>
nnoremap <S-right> <Esc>v<right>
vnoremap <S-down> <down>
vnoremap <S-up> <up>
vnoremap <S-left> <left>
vnoremap <S-right> <right>

" En modo Vision poder eliminar texto
" Space / Enter / Delete / Backspace
vnoremap <Space> di<space>
vnoremap <Enter> di<enter>
vnoremap <Delete> di
vnoremap <Backspace> di

" Selecionar desde el puntero al comienzo o final de linea
" Shift Home / Shift End
inoremap <S-Home> <Esc>v0
inoremap <S-End> <Esc>v$

" Cambia al modo Insert cuando sueltas el Shift
vnoremap <down> <esc>i<down>
vnoremap <up> <esc>i<up>
vnoremap <left> <esc>i<left>
vnoremap <right> <esc>i<right>
vnoremap <home> <esc>i<home>
vnoremap <end> <esc>i<end>

" Copia, Cortar, Pegar
" Ctrl C (Visual)
" Ctrl X (Visual)
" Ctrl V (Normal y Insert)
vnoremap <C-c> x:undo<CR>i
vnoremap <C-x> xh<CR>i
nnoremap <C-v> p<CR>i
inoremap <C-v> <Esc>p<CR>i

" Detecta y cambia modo edicion
" Space / Enter / Delete / Backspace
nnoremap <Space> i<space>
nnoremap <Enter> i<enter>
nnoremap <Delete> i<Delete>
nnoremap <Backspace> i<backspace>
